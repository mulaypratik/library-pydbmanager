'''
Created on Apr 15, 2018

@author: Pratik
'''
from DBManager import DBManager


class ConstantsClassDB:
    'This class contains an object of DBManager class which can be used as a static throughout the project.'
    
    dbManager = DBManager(None, None, None, None, None)
