'''
Created on Apr 14, 2018

@author: Pratik
'''
import pymssql


class DBManager:
    'This class contains all the database related operations.'
    
    print "Inside DBManager"
    
    def __init__(self, DBServerIP, DBUserName, DBPassword, DBName, DBType):
        print "Inside DBManager constructor"
        
        self.__DBServerIP = DBServerIP
        self.__DBUserName = DBUserName
        self.__DBPassword = DBPassword
        self.__DBName = DBName
        self.__DBType = DBType

    def connectToDB(self):
        try:
            self.__conn = pymssql.connect(host=self.__DBServerIP, user=self.__DBUserName, password=self.__DBPassword, database=self.__DBName)
            print "Connected to DB."
            
            return True
        except Exception as e:
            print e
            
            return False
        
    def getStringFromDB(self, Query):
        try:
            __cursor = self.__conn.cursor()
            __cursor.execute(Query)
            row = __cursor.fetchone()
            
            while row:
                return str(row[0])
        except Exception as e:
            print e
            
            return e
            
    def getDataFromDB(self, Query):
        try:
            __cursor = self.__conn.cursor()
            __cursor.execute(Query)
            __data = __cursor.fetchall()
            __cursor.close()
            
            return __data
        except Exception as e:
            print e
            
            return e
        
    def executeQuery(self, Query):
        try:
            __cursor = self.__conn.cursor()
            __cursor.execute(Query)
            self.__conn.commit()
            
            return None
        except Exception as e:
            self.__conn.rollback()
            print e
            
            return e
        
    def executeTransaction(self, Queries):
        try:
            __cursor = self.__conn.cursor()
            
            for Query in Queries:
                __cursor.execute(Query)
            
            self.__conn.commit()
            
            return None
        except Exception as e:
            self.__conn.rollback()
            print e
            
            return e
        
    def updateImage(self, Query, ImagePath):
        try:
            __cursor = self.__conn.cursor()
            
            with open(ImagePath, 'rb') as imageFile:
                __image = imageFile.read()
            
            __cursor.execute(Query, __image)
            self.__conn.commit()
            
            return None
        except Exception as e:
            self.__conn.rollback()
            print e
            
            return e
    
    def closeConnection(self):
        try:
            self.__conn.close()
            
            return None
        except Exception as e:
            print e
            
            return e
