'''
Created on Apr 15, 2018

@author: Pratik
'''
import os
from DB.ConstantsClassDB import ConstantsClassDB


class DBTester:
    'DBTester class to test various operations of DBManager from outside'

    def __init__(self):
        # Initialize DBManager and connect to DB
        ConstantsClassDB.dbManager.__init__("localhost", "sa", "sscs@password", "DB_CCMS", "MSSQL")
    
        isDBConnected = ConstantsClassDB.dbManager.connectToDB()
    
        if isDBConnected:
            # Print single string query result
            print "\nAdmin_Id: ", ConstantsClassDB.dbManager.getStringFromDB("SELECT DISTINCT Admin_Id FROM Tbl_Admin_Master")
            
            # Print entire query result
            print "\nTable Data:"
            data = ConstantsClassDB.dbManager.getDataFromDB("SELECT DISTINCT Admin_Id, First_Name + ' ' + Last_Name, Mob_No FROM Tbl_Admin_Master")
            for row in data:
                print row[0], "|", row[1], "|", row[2]
            
            # Execute Update query
            print "\nExecute update query:", ConstantsClassDB.dbManager.executeQuery("UPDATE Tbl_Admin_Master SET Admin_Id = 'A0003' where Admin_Id = 'A0002'")
            
            # Execute Update transaction
            print "\nExecute transaction:", ConstantsClassDB.dbManager.executeTransaction(["UPDATE Tbl_Admin_Master SET Admin_Id = 'A0001' WHERE Admin_Id = 'A0004'", "UPDATE Tbl_Admin_Master SET Mod_Timestamp = 'A0002'", "UPDATE Tbl_Admin_Master SET Admin_Id = 'A0002' WHER Admin_Id = 'A0003'"])    
            
            # Update BLOB image
            print "\nImage update:", ConstantsClassDB.dbManager.updateImage("UPDATE Tbl_Admin_Master SET Photo = %s WHERE Admin_Id = 'A0003' AND isDeleted <> '1'", (os.path.dirname(os.path.realpath(__file__)) + "\\res\\img\\Leo.jpg"))
            
            # Close DB connection
            ConstantsClassDB.dbManager.closeConnection()
            
            # Testing after DB connection closed
            print "\nFirst_Name: ", ConstantsClassDB.dbManager.getStringFromDB("SELECT DISTINCT First_Name FROM Tbl_Admin_Master;")
        else:
            print "\nDB Connection failed!"


# Call DBTester
dbt = DBTester()
